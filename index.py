import os
import webapp2
from google.appengine.ext import db
from google.appengine.ext.webapp import template
from models.Hook import User

def gpath(name):
	return os.path.join(os.path.dirname(__file__), name)
	
badlogin = gpath('badlogin.html')
class LoginPage(webapp2.RequestHandler):
	def get(self):
		self.response.out.write(template.render(gpath('login.html'),{}))
		return

class LoginCheck(webapp2.RequestHandler):
	def get(self):
		user = self.request.params.get('user')
		password = self.request.params.get('password')	
		userobj = User.get_by_key_name(user)
		if userobj is None or password != userobj.password:
			returnvals = { 'user' : user, 'password' : password }
			newuser = User(key_name=user)
			newuser.hookset = []
			newuser.password = password
			newuser.put()
			self.response.out.write(template.render(badlogin,returnvals))
			return
		userhooks = userobj.hooksent
		template_values = {
			'user' : user,
			'hooks' : userhooks
		}
		self.response.out.write(template.render(gpath('index.html'),template_values))

class HookMitPage(webapp2.RequestHandler):
	def post(self):
		user = self.request.params.get('user')
		hooktext = self.request.params.get('text')
		userobj = User.get_by_key_name(user)
		userhooks = userobj.hooksent
		userhooks.append(db.Text(hooktext))
		template_values = {
			'user' : user,
			'hooks' : userhooks
		}
		userobj.put()
		self.response.out.write(template.render(gpath('index.html'),template_values))

class HookPage(webapp2.RequestHandler):
	def get(self):
		user = self.request.params.get('user')
		userobj = User.get_by_key_name(user)
		userhooks = userobj.hooksent
		template_values = {
			'user' : user,
			'hooks' : userhooks
		}
		self.response.out.write(template.render(gpath('index.html'),template_values))

app = webapp2.WSGIApplication([	
				('/hookmit.*',HookMitPage),
				('/hook', HookPage),
				('/login.*', LoginCheck),
				('/.*', LoginPage )
			])

def main():
	app.run()

if __name__ == '__main__':
	main()


		
